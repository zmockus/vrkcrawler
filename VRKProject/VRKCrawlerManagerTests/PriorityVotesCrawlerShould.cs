﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VRKCrawlerManager.Services.ParliamentElections;

namespace VRKCrawlerManagerTests
{
    [TestClass]
    public class PriorityVotesCrawlerShould
    {
        [TestMethod]
        public void GetPriorityVotesResults()
        {
            var elections = new ParliamentElectionsRound1PriorityVotesCrawlerService();
            var dataDictionary = new KeyValuePair<string, string>(string.Empty,
                @"C:\Projects\Kursinis\VRKProject\VRKCrawlerManagerTests\TestData\PriorityVotesTestData.htm");


            var results = elections.CreateCandidateList(dataDictionary);

            Assert.AreEqual(70, results.Candidates[0].AmountOfPriorityVotes);
            Assert.AreEqual(1, results.Candidates[0].RankAfterElections);
            Assert.AreEqual(1, results.Candidates[0].RankBeforeElections);
            Assert.AreEqual("Eligijus MASIULIS (V)", results.Candidates[0].Name);
            Assert.AreEqual("Lietuvos Respublikos liberalų sąjūdis", results.Candidates[0].Party);

            Assert.AreEqual(24, results.Candidates[2].AmountOfPriorityVotes);
            Assert.AreEqual(3, results.Candidates[2].RankAfterElections);
            Assert.AreEqual(6, results.Candidates[2].RankBeforeElections);
            Assert.AreEqual("Eugenijus GENTVILAS", results.Candidates[2].Name);
            Assert.AreEqual("Lietuvos Respublikos liberalų sąjūdis", results.Candidates[2].Party);
        }
    }
}
