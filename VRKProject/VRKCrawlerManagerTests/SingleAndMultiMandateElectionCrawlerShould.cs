﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VRKCrawlerManager.Services.PresidentElections;

namespace VRKCrawlerManagerTests
{
    [TestClass]
    public class SingleAndMultiMandateElectionCrawlerShould
    {
        [TestMethod]
        public void GetCorrectResults()
        {
            var electionService = new PresidentElectionsRound2CrawlerService();

            var results = electionService.ParseVicinityResults(@"C:\Projects\Kursinis\VRKProject\VRKCrawlerManagerTests\TestData\SingleMandateTestData.htm");

            Assert.AreEqual("Akmenės rajono (Nr.1) savivaldybė Ramučių  (Nr.4) apylinkė  ", results.Description);
            Assert.AreEqual(15, results.TotalAmountOfInvalidVotes);
            Assert.AreEqual(631, results.TotalAmountOfValidVotes);
            Assert.AreEqual(1408, results.TotalAmountOfVoters);
            Assert.AreEqual(646, results.TotalAmountOfVotes);

            Assert.AreEqual("Dalia GRYBAUSKAITĖ", results.ListOfCandidates[0].Name);
            Assert.AreEqual(298, results.ListOfCandidates[0].AmountOfVotesGivenInDistricts);
            Assert.AreEqual(47, results.ListOfCandidates[0].AmountOfVotesGivenUsingPost);
            Assert.AreEqual(345, results.ListOfCandidates[0].TotalAmountOfVotesForACandidate);

            Assert.AreEqual("Zigmantas BALČYTIS", results.ListOfCandidates[1].Name);
            Assert.AreEqual(264, results.ListOfCandidates[1].AmountOfVotesGivenInDistricts);
            Assert.AreEqual(22, results.ListOfCandidates[1].AmountOfVotesGivenUsingPost);
            Assert.AreEqual(286, results.ListOfCandidates[1].TotalAmountOfVotesForACandidate);
        }
    }
}
