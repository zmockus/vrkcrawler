﻿using VRKCrawlerManager.Model.MultimandateElections.PriorityVotes;

namespace VRKCrawlerManager.Interfaces
{
    public interface IPriorityVotesCrawlerService
    {
        PriorityVotes Start(string pathOnDisk);
    }
}
