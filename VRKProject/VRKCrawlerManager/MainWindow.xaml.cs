﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using VRKCrawlerManager.Interfaces;
using VRKCrawlerManager.Services.ParliamentElections;
using VRKCrawlerManager.Services.PresidentElections;
using VRKCrawlerManager.Windows;

namespace VRKCrawlerManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Get2008MultimandateResultsRound1Button_Click(object sender, RoutedEventArgs e)
        {
            var rootUrl = "http://www.vrk.lt/statiniai/puslapiai/2008_seimo_rinkimai/output_lt/rezultatai_daugiamand_apygardose/rezultatai_daugiamand_apygardose1turas.html";
            var rootUrlEmbassies = "http://www.vrk.lt/statiniai/puslapiai/2008_seimo_rinkimai/output_lt/rezultatai_daugiamand_apygardose/balsavimasatstovybese1turas.html";
            var failureMessage = "Nepavyko surinkti 2008 metų parlamentų rinkimų I turo (daugiamandatės apygardos) rezultatų.";
            var successMessage = "Pavyko surinkti 2008 metų parlamentų rinkimų I turo (daugiamandatės apygardos) rezultatus.";

            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new ParliamentElectionsMultimandateRound1CrawlerService(rootUrl, rootUrlEmbassies, "2008", 16, cancellationToken.Token);
            
            DisableButton(Get2008MultimandateResultsRound1Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2008MultimandateResultsRound1Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show();
                window.Hide();
                EnableButton(Get2008MultimandateResultsRound1Button);
                return;
            }

            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2008MultimandateResultsRound1Button);
        }

        private async void Get2008SinglemandateResultsRound1Button_Click(object sender, RoutedEventArgs e)
        {
            var rootUrl = "http://www.vrk.lt/statiniai/puslapiai/2008_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose/rezultatai_vienmand_apygardose1turas.html";
            var rootUrlEmbassies = "http://www.vrk.lt/statiniai/puslapiai/2008_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose/atstovybesaktyvumasdesc1turas.html";
            var failureMessage = "Nepavyko surinkti 2008 metų parlamentų rinkimų I turo (vienmandatės apygardos) rezultatų.";
            var successMessage = "Pavyko surinkti 2008 metų parlamentų rinkimų I turo (vienmandatės apygardos) rezultatus.";

            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new ParliamentElectionsSinglemandateRound1CrawlerService(rootUrl, rootUrlEmbassies, "2008", cancellationToken.Token);

            DisableButton(Get2008SinglemandateResultsRound1Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2008SinglemandateResultsRound1Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show();
                window.Hide();
                EnableButton(Get2008SinglemandateResultsRound1Button);
                return;
            }

            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2008SinglemandateResultsRound1Button);
        }

        private async void Get2008SinglemandateResultsRound2Button_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Add UTs
            var rootUrl = "http://www.vrk.lt/statiniai/puslapiai/2008_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose2/rezultatai_vienmand_apygardose2turas.html";
            var rootUrlEmbassies = "http://www.vrk.lt/statiniai/puslapiai/2008_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose2/atstovybesaktyvumasdesc2turas.html";
            var failureMessage = "Nepavyko surinkti 2008 metų parlamentų rinkimų II turo (vienmandatės apygardos) rezultatų.";
            var successMessage = "Pavyko surinkti 2008 metų parlamentų rinkimų II turo (vienmandatės apygardos) rezultatus.";
            
            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new ParliamentElectionsRound2CrawlerService(rootUrl, rootUrlEmbassies, "2008", cancellationToken.Token);

            DisableButton(Get2008SinglemandateResultsRound2Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2008SinglemandateResultsRound2Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show();
                window.Hide();
                EnableButton(Get2008SinglemandateResultsRound2Button);
                return;
            }
            
            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2008SinglemandateResultsRound2Button);

        }

        private async void Get2012MultimandateResultsRound1Button_Click(object sender, RoutedEventArgs e)
        {
            var rootUrl = "http://www.vrk.lt/statiniai/puslapiai/2012_seimo_rinkimai/output_lt/rezultatai_daugiamand_apygardose/rezultatai_daugiamand_apygardose1turas.html";
            var rootUrlEmbassies = "http://www.vrk.lt/statiniai/puslapiai/2012_seimo_rinkimai/output_lt/rezultatai_daugiamand_apygardose/balsavimasatstovybese1turas.html";
            var failureMessage = "Nepavyko surinkti 2012 metų parlamentų rinkimų I turo (daugiamandatės apygardos) rezultatų.";
            var successMessage = "Pavyko surinkti 2012 metų parlamentų rinkimų I turo (daugiamandatės apygardos) rezultatus.";

            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new ParliamentElectionsMultimandateRound1CrawlerService(rootUrl, rootUrlEmbassies, "2012", 18, cancellationToken.Token);

            DisableButton(Get2012MultimandateResultsRound1Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2012MultimandateResultsRound1Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show();
                window.Hide();
                EnableButton(Get2012MultimandateResultsRound1Button);
                return;
            }

            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2012MultimandateResultsRound1Button);
        }

        private async void Get2012SinglemandateResultsRound1Button_Click(object sender, RoutedEventArgs e)
        {
            var rootUrl = "http://www.vrk.lt/statiniai/puslapiai/2012_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose/rezultatai_vienmand_apygardose1turas.html";
            var rootUrlEmbassies = "http://www.vrk.lt/statiniai/puslapiai/2012_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose/atstovybesaktyvumasdesc1turas.html";
            var failureMessage = "Nepavyko surinkti 2012 metų parlamentų rinkimų I turo (vienmandatės apygardos) rezultatų.";
            var successMessage = "Pavyko surinkti 2012 metų parlamentų rinkimų I turo (vienmandatės apygardos) rezultatus.";

            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new ParliamentElectionsSinglemandateRound1CrawlerService(rootUrl, rootUrlEmbassies, "2012", cancellationToken.Token);

            DisableButton(Get2012SinglemandateResultsRound1Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2012SinglemandateResultsRound1Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show();
                window.Hide();
                EnableButton(Get2012SinglemandateResultsRound1Button);
                return;
            }

            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2012SinglemandateResultsRound1Button);
        }

        private async void Get2012SinglemandateResultsRound2Button_Click(object sender, RoutedEventArgs e)
        {
            var rootUrl = "http://www.vrk.lt/statiniai/puslapiai/2012_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose2/rezultatai_vienmand_apygardose2turas.html";
            var rootUrlEmbassies = "http://www.vrk.lt/statiniai/puslapiai/2012_seimo_rinkimai/output_lt/rezultatai_vienmand_apygardose2/atstovybesaktyvumasdesc2turas.html";
            var failureMessage = "Nepavyko surinkti 2012 metų parlamentų rinkimų I turo (vienmandatės apygardos) rezultatų.";
            var successMessage = "Pavyko surinkti 2012 metų parlamentų rinkimų I turo (vienmandatės apygardos) rezultatus.";

            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new ParliamentElectionsRound2CrawlerService(rootUrl, rootUrlEmbassies, "2012", cancellationToken.Token);

            DisableButton(Get2012SinglemandateResultsRound2Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2012SinglemandateResultsRound2Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show();
                window.Hide();
                EnableButton(Get2012SinglemandateResultsRound2Button);
                return;
            }

            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2012SinglemandateResultsRound2Button);
        }

        private async void Get2014ResultsRound1Button_Click(object sender, RoutedEventArgs e)
        {
            var rootUrl = "http://www.2013.vrk.lt/2014_prezidento_rinkimai/output_lt/rezultatai_vienmand_apygardose/rezultatai_vienmand_apygardose1turas.html";
            var municipalityUrl = "http://www.2013.vrk.lt/2014_prezidento_rinkimai/output_lt/rezultatai_vienmand_apygardose/{0}";
            var vicinityUrl = "http://www.2013.vrk.lt/2014_prezidento_rinkimai/output_lt/rezultatai_vienmand_apygardose/{0}";
            var failureMessage = "Nepavyko surinkti 2014 metų prezidento rinkimų I turo rezultatų.";
            var successMessage = "Pavyko surinkti 2014 metų prezidento rinkimų I turo rezultatus.";

            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new PresidentElectionsRound1CrawlerService(rootUrl, municipalityUrl, vicinityUrl, "2014", 7, cancellationToken.Token);

            DisableButton(Get2014ResultsRound1Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2014ResultsRound1Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show(); 
                window.Hide();
                EnableButton(Get2014ResultsRound1Button);
                return;
            }

            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2014ResultsRound1Button);
        }

        private async void Get2014ResultsRound2Button_Click(object sender, RoutedEventArgs e)
        {
            var rootUrl = "http://www.2013.vrk.lt/2014_prezidento_rinkimai/output_lt/rezultatai_vienmand_apygardose2/rezultatai_vienmand_apygardose2turas.html";
            var municipalityUrl = "http://www.2013.vrk.lt/2014_prezidento_rinkimai/output_lt/rezultatai_vienmand_apygardose2/{0}";
            var vicinityUrl = "http://www.2013.vrk.lt/2014_prezidento_rinkimai/output_lt/rezultatai_vienmand_apygardose2/{0}";
            var failureMessage = "Nepavyko surinkti 2014 metų prezidento rinkimų II turo rezultatų.";
            var successMessage = "Pavyko surinkti 2014 metų prezidento rinkimų II turo rezultatus.";

            var cancellationToken = new CancellationTokenSource();
            var window = new Progress(cancellationToken);
            ICrawlerService service = new PresidentElectionsRound2CrawlerService(rootUrl, municipalityUrl, vicinityUrl, "2014", cancellationToken.Token);

            DisableButton(Get2014ResultsRound2Button);
            window.Show();

            try
            {
                await Task.Run(() => service.Start(), cancellationToken.Token);
            }
            catch (OperationCanceledException)
            {
                window.Hide();
                EnableButton(Get2014ResultsRound2Button);
                return;
            }
            catch (Exception)
            {
                var error = new Failure(failureMessage);
                error.Show();
                window.Hide();
                EnableButton(Get2014ResultsRound2Button);
                return;
            }

            var success = new Success(successMessage);
            success.Show();

            window.Hide();
            EnableButton(Get2014ResultsRound2Button);
        }

        private void EnableButton(Button button)
        {
            button.IsEnabled = true;
        }

        private void DisableButton(Button button)
        {
            button.IsEnabled = false;
        }
    }
}
