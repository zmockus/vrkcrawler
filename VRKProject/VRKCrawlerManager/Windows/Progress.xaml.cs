﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VRKCrawlerManager
{
    /// <summary>
    /// Interaction logic for Progress.xaml
    /// </summary>
    public partial class Progress : Window
    {
        private CancellationTokenSource cancellationToken;

        public Progress(CancellationTokenSource cancellationToken)
        {
            this.cancellationToken = cancellationToken;
            InitializeComponent();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            cancellationToken.Cancel();
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            cancellationToken.Cancel();
        }
    }
}
