﻿namespace VRKCrawlerManager.Model.SinglemandateElections
{
    public class CandidateResults
    {
        public string Name { get; set; }
        public string Municipality { get; set; }
        public string Vicinity { get; set; }
        public int AmountOfVotesGivenUsingPost { get; set; }

        public int AmountOfVotesGivenInDistricts { get; set; }

        public int TotalAmountOfVoters { get; set; }

        public int TotalAmountOfValidVotes { get; set; }

        public int TotalAmountOfInvalidVotes { get; set; }

        public int TotalAmountOfVotesForACandidate
        {
            get { return AmountOfVotesGivenInDistricts + AmountOfVotesGivenUsingPost; }
        }

        public decimal PercentageOfValidVotes
        {
            get { return ((decimal)AmountOfVotesGivenInDistricts + AmountOfVotesGivenUsingPost) / TotalAmountOfValidVotes * 100; }
        }

        public decimal PercentageOfVotes
        {
            get { return (((decimal)AmountOfVotesGivenInDistricts + AmountOfVotesGivenUsingPost) / (TotalAmountOfValidVotes + TotalAmountOfInvalidVotes)) * 100; }
        }
    }
}
