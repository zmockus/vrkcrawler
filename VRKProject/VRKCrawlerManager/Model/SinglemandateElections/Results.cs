﻿using System.Collections.Generic;

namespace VRKCrawlerManager.Model.SinglemandateElections
{
    public class Results
    {
        public string Description { get; set; }
        public int TotalAmountOfVoters { get; set; }
        public int TotalAmountOfVotes { get; set; }

        public decimal TotalPercentageOfVotes
        {
            get { return (decimal)TotalAmountOfVotes / TotalAmountOfVoters * 100; }
        }

        public int TotalAmountOfInvalidVotes { get; set; }

        public decimal TotalPercentageOfInvalidVotes
        {
            get { return (decimal)TotalAmountOfInvalidVotes / TotalAmountOfVotes * 100; }
        }

        public int TotalAmountOfValidVotes { get; set; }

        public decimal TotalPercentageOfValidVotes
        {
            get { return (decimal)TotalAmountOfValidVotes / TotalAmountOfVotes * 100; }
        }

        public List<CandidateResults> ListOfCandidates { get; set; }

        public Results()
        {
            ListOfCandidates = new List<CandidateResults>();
        }
    }
}
