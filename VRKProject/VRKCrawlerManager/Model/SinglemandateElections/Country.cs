﻿using System.Collections.Generic;

namespace VRKCrawlerManager.Model.SinglemandateElections
{
    public class Country
    {
        public Results Results { get; set; }
        public Municipality ResultsFromEmbassies { get; set; }
        public List<Municipality> ListOfMunicipalities { get; set; } 
    }
}
