﻿using System.Collections.Generic;

namespace VRKCrawlerManager.Model.SinglemandateElections
{
    public class Municipality
    {
        public Results Results { get; set; }
        public List<Vicinity> ListOfVicinities { get; set; } 
    }
}