﻿using System.Dynamic;
using System.Runtime.Serialization;
using ServiceStack.Text;

namespace VRKCrawlerManager.Model.MultimandateElections
{
    public class PartyResults
    {
        public string Name { get; set; }
        public string Municipality { get; set; }
        public string Vicinity { get; set; }
        public int AmountOfVotesGivenUsingPost { get; set; }

        public int AmountOfVotesGivenInDistricts { get; set; }

        public int TotalAmountOfVoters { get; set; }

        public int TotalAmountOfValidVotes { get; set; }
        public int TotalAmountOfInvalidVotes { get; set; }

        [IgnoreDataMember]
        public int AmountOfMandatesReceived { get; set; }

        public int TotalAmountOfVotesForACandidate
        {
            get { return AmountOfVotesGivenInDistricts + AmountOfVotesGivenUsingPost; }
        }

        public decimal PercentageOfValidVotes
        {
            get { return ((decimal)AmountOfVotesGivenInDistricts + AmountOfVotesGivenUsingPost) / TotalAmountOfValidVotes * 100; }
        }

        public decimal PercentageOfVoters
        {
            get { return (((decimal)AmountOfVotesGivenInDistricts + AmountOfVotesGivenUsingPost) / (TotalAmountOfValidVotes + TotalAmountOfInvalidVotes)) * 100; }
        }

        public PartyResults()
        {
        }

        public PartyResults(string name, int amountOfVotesGivenUsingPost, int amountOfVotesGivenInDistricts, int totalAmountOfVoters, int totalAmountOfValidVotes, int amountOfMandatesReceived)
        {
            Name = name;
            AmountOfVotesGivenInDistricts = amountOfVotesGivenInDistricts;
            AmountOfVotesGivenUsingPost = amountOfVotesGivenUsingPost;
            TotalAmountOfVoters = totalAmountOfVoters;
            TotalAmountOfValidVotes = totalAmountOfValidVotes;
            AmountOfMandatesReceived = amountOfMandatesReceived;
        }
    }
}
