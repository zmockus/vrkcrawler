﻿using System.Collections.Generic;

namespace VRKCrawlerManager.Model.MultimandateElections
{
    public class Municipality
    {
        public PriorityVotes.PriorityVotes PriorityVotes { get; set; }
        public Results Results { get; set; }
        public List<Vicinity> ListOfVicinities { get; set; }

        public Municipality()
        {
            ListOfVicinities = new List<Vicinity>();
        }
    }
}
