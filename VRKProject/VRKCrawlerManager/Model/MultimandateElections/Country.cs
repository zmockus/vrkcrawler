﻿using System.Collections.Generic;

namespace VRKCrawlerManager.Model.MultimandateElections
{
    public class Country
    { 
        public PriorityVotes.PriorityVotes PriorityVotes { get; set; }
        public Results Results { get; set; }
        public List<Municipality> Municipalities { get; set; }
        public Municipality Embassies { get; set; } 
    }
}
