﻿using System.Collections.Generic;

namespace VRKCrawlerManager.Model.MultimandateElections.PriorityVotes
{
    public class PriorityVotes
    {
        public List<Party> ListOfParties;

        public PriorityVotes()
        {
            ListOfParties = new List<Party>();
        }
    }
}
