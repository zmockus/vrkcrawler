﻿using System.Collections.Generic;

namespace VRKCrawlerManager.Model.MultimandateElections.PriorityVotes
{
    public class Party
    {
        public List<Candidate> Candidates { get; set; }

        public Party(List<Candidate> candidates)
        {
            Candidates = candidates;
        }
    }
}
