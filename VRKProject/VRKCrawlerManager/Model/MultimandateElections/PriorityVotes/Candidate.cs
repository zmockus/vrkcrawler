﻿namespace VRKCrawlerManager.Model.MultimandateElections.PriorityVotes
{
    public class Candidate
    {
        public string Name { get; private set; }
        public string Party { get; private set; }
        public string Municipality { get; private set; }
        public string Vicinity { get; private set; }
        public int RankAfterElections { get; private set; }
        public int RankBeforeElections { get; private set; }
        public int AmountOfPriorityVotes { get; private set; }
        public int AmountOfPoints { get; private set; }

        public Candidate(int rankBeforeElections, int rankAfterElections, string name, int amountOfPriorityVotes, int amountOfPoints, string party, string municipality = null, string vicinity = null)
        {
            RankAfterElections = rankAfterElections;
            RankBeforeElections = rankBeforeElections;
            Name = name;
            AmountOfPoints = amountOfPoints;
            AmountOfPriorityVotes = amountOfPriorityVotes;
            Party = party;
            Vicinity = vicinity;
            Municipality = municipality;
        }
    }
}
