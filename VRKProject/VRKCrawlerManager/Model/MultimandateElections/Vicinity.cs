﻿namespace VRKCrawlerManager.Model.MultimandateElections
{
    public class Vicinity
    {
        public PriorityVotes.PriorityVotes PriorityVotes { get; set; }
        public Results Results { get; set; }
    }
}
