﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using VRKCrawlerManager.Model.MultimandateElections;
using VRKCrawlerManager.Model.MultimandateElections.PriorityVotes;
using VRKCrawlerManager.Model.SinglemandateElections;
using Country = VRKCrawlerManager.Model.SinglemandateElections.Country;

namespace VRKCrawlerManager.Helpers
{
    public class CrawlerHelpers
    {
        private CancellationToken cancellationToken;
        private readonly ILog log;
        private readonly Random randomNumberGenerator;

        public CrawlerHelpers(CancellationToken cancellationToken)
        {
            this.cancellationToken = cancellationToken;
            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(GetType());
            randomNumberGenerator = new Random();  
        }

        public string GetFilePathOnDisk(int sessionHash)
        {
            var documentHash = randomNumberGenerator.Next(0, 100000001);
            CreateDirectoryIfItDoesNotExist(sessionHash);
            var filePathOnDisk = string.Format("{0}\\{1}\\{2}.html", Environment.GetEnvironmentVariable("temp"), sessionHash, documentHash);
            if (File.Exists(filePathOnDisk))
            {
                GetFilePathOnDisk(sessionHash);
            }

            return filePathOnDisk;
        }

        public void CreateDirectoryIfItDoesNotExist(int sessionHash)
        {
            var tempPathWithSessionHash = string.Format("{0}\\{1}", Environment.GetEnvironmentVariable("temp"), sessionHash);
            if (!Directory.Exists(tempPathWithSessionHash))
            {
                var securityRules = new DirectorySecurity();
                securityRules.AddAccessRule(new FileSystemAccessRule(WindowsIdentity.GetCurrent().Name, FileSystemRights.FullControl, AccessControlType.Allow));
                Directory.CreateDirectory(tempPathWithSessionHash, securityRules);
            }
        }

        public void DownloadPages(Dictionary<string, string> dictionary)
        {
            Console.WriteLine("Started downloading separate files");
            log.Info("Started downloading separate files");
            var amountOfProblems = 0;
            var missingFilesDictionary = new Dictionary<string, string>();

            Parallel.ForEach(dictionary, new ParallelOptions { MaxDegreeOfParallelism = 15 }, (keyValuePair, state) =>
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    state.Stop();
                    return;    
                }

                var isDownloaded = false;
                using (var webClient = new WebClient())
                {
                    for (var amountOfRetries = 0; amountOfRetries < 5; amountOfRetries++)
                    {
                        try
                        {
                            Thread.Sleep(1000);
                            webClient.DownloadFile(keyValuePair.Key, keyValuePair.Value);
                            Console.WriteLine("Finished downloading file to: {0}", keyValuePair.Value);
                            log.Info(string.Format("Finished downloading file from {0} to {1}", keyValuePair.Key, keyValuePair.Value));
                            isDownloaded = true;
                            return;
                        }
                        catch (WebException)
                        {
                            Console.WriteLine("Exception thrown when downloading file: {0}", keyValuePair.Key);
                            log.Error(string.Format("Exception thrown when downloading file from {0} to {1}", keyValuePair.Key, keyValuePair.Value));
                        }
                    }

                    if (!isDownloaded)
                    {
                        amountOfProblems++;
                        missingFilesDictionary.Add(keyValuePair.Key, keyValuePair.Value);
                    }
                }
            });

            if (amountOfProblems > 0)
            {
                Console.WriteLine("Articles were not downloaded successfully. {0} files are missing", amountOfProblems);
                log.Error(string.Format("Articles were not downloaded successfully. {0} files are missing", amountOfProblems));
                Console.WriteLine("Missing files list: ");
                log.Error("Missing files list: ");
                foreach (var keyValuePair in missingFilesDictionary)
                {
                    Console.WriteLine("{0} - {1}", keyValuePair.Key, keyValuePair.Value);
                    log.Error(string.Format("{0} - {1}", keyValuePair.Key, keyValuePair.Value));
                }
            }
            else
            {
                Console.WriteLine("Articles were downloaded successfully.");
                log.Info("Articles were downloaded successfully");
            }
        }

        public static List<CandidateResults> CreateCsvListSiglemandate(Country countryResults)
        {
            var candidateResults = new List<CandidateResults>();
            foreach (var municipality in countryResults.ListOfMunicipalities)
            {
                foreach (var vicinity in municipality.ListOfVicinities)
                {
                    candidateResults.AddRange(vicinity.Results.ListOfCandidates.Select(result => new CandidateResults
                    {
                        AmountOfVotesGivenInDistricts = result.AmountOfVotesGivenInDistricts, AmountOfVotesGivenUsingPost = result.AmountOfVotesGivenUsingPost,
                        Name = result.Name, TotalAmountOfValidVotes = vicinity.Results.TotalAmountOfValidVotes, TotalAmountOfVoters = vicinity.Results.TotalAmountOfVoters,
                        Municipality = municipality.Results.Description, Vicinity = vicinity.Results.Description.Replace(string.Format("{0} ", municipality.Results.Description), ""),
                        TotalAmountOfInvalidVotes = vicinity.Results.TotalAmountOfInvalidVotes
                    }));
                }
            }

            if (countryResults.ResultsFromEmbassies != null)
            {
                foreach (var vicinity in countryResults.ResultsFromEmbassies.ListOfVicinities)
                {
                    candidateResults.AddRange(vicinity.Results.ListOfCandidates.Select(result => new CandidateResults
                    {
                        AmountOfVotesGivenInDistricts = result.AmountOfVotesGivenInDistricts,
                        AmountOfVotesGivenUsingPost = result.AmountOfVotesGivenUsingPost,
                        Name = result.Name,
                        TotalAmountOfValidVotes = vicinity.Results.TotalAmountOfValidVotes,
                        TotalAmountOfVoters = vicinity.Results.TotalAmountOfVoters,
                        Municipality = "Balsavimas LR diplomatinėse atstovybėse",
                        Vicinity = vicinity.Results.Description.Replace("Balsavimas LR diplomatinėse atstovybėse ", ""),
                        TotalAmountOfInvalidVotes = vicinity.Results.TotalAmountOfInvalidVotes
                    }));
                }
            }

            return candidateResults;
        }

        public static List<PartyResults> CreateCsvListMultimandate(Model.MultimandateElections.Country countryResults)
        {
            var partyResults = new List<PartyResults>();
            foreach (var municipality in countryResults.Municipalities)
            {
                foreach (var vicinity in municipality.ListOfVicinities)
                {
                    foreach (var result in vicinity.Results.ListOfParties)
                    {
                        var partyResult = new PartyResults
                        {
                            AmountOfVotesGivenInDistricts = result.AmountOfVotesGivenInDistricts,
                            AmountOfVotesGivenUsingPost = result.AmountOfVotesGivenUsingPost,
                            Name = result.Name,
                            TotalAmountOfValidVotes = vicinity.Results.TotalAmountOfValidVotes,
                            TotalAmountOfVoters = vicinity.Results.TotalAmountOfVoters,
                            Municipality = municipality.Results.Description,
                            Vicinity =
                                vicinity.Results.Description.Replace(
                                    string.Format("{0} ", municipality.Results.Description), ""),
                            TotalAmountOfInvalidVotes = vicinity.Results.TotalAmountOfInvalidVotes
                        };

                        partyResults.Add(partyResult);
                    }
                }
            }

            if (countryResults.Embassies != null)
            {
                foreach (var vicinity in countryResults.Embassies.ListOfVicinities)
                {
                    partyResults.AddRange(vicinity.Results.ListOfParties.Select(result => new PartyResults
                    {
                        AmountOfVotesGivenInDistricts = result.AmountOfVotesGivenInDistricts, AmountOfVotesGivenUsingPost = result.AmountOfVotesGivenUsingPost,
                        Name = result.Name, TotalAmountOfValidVotes = vicinity.Results.TotalAmountOfValidVotes, TotalAmountOfVoters = vicinity.Results.TotalAmountOfVoters,
                        Municipality = "Balsavimas LR diplomatinėse atstovybėse", Vicinity = vicinity.Results.Description.Replace("Balsavimas LR diplomatinėse atstovybėse ", ""),
                        TotalAmountOfInvalidVotes = vicinity.Results.TotalAmountOfInvalidVotes
                    }));
                }
            }

            return partyResults;
        }

        public static List<Candidate> GetCsvListPriorityVotes(Model.MultimandateElections.Country countryResults)
        {
            var candidateList = new List<Candidate>();

            foreach (var municipality in countryResults.Municipalities)
            {
                foreach (var vicinity in municipality.ListOfVicinities)
                {
                    if (vicinity.PriorityVotes != null)
                    {
                        foreach (var party in vicinity.PriorityVotes.ListOfParties)
                        {
                            foreach (var candidate in party.Candidates)
                            {
                                var candidateItem = new Candidate(candidate.RankBeforeElections, candidate.RankAfterElections,
                                    candidate.Name, candidate.AmountOfPriorityVotes, candidate.AmountOfPoints,
                                    candidate.Party, municipality.Results.Description, vicinity.Results.Description.Replace(string.Format("{0} ", municipality.Results.Description), ""));
                                candidateList.Add(candidateItem);
                            }
                        }
                    }
                }
            }

            return candidateList;
        }
    }
}
