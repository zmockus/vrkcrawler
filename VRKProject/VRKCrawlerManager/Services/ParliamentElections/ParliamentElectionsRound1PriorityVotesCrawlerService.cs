﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using VRKCrawlerManager.Helpers;
using VRKCrawlerManager.Interfaces;
using VRKCrawlerManager.Model.MultimandateElections.PriorityVotes;

namespace VRKCrawlerManager.Services.ParliamentElections
{
    public class ParliamentElectionsRound1PriorityVotesCrawlerService : IPriorityVotesCrawlerService
    {
        private static readonly HtmlDocument htmlDocument = new HtmlDocument();
        private CrawlerHelpers crawlerHelpers = new CrawlerHelpers(new CancellationToken());
        private readonly Random randomNumberGenerator = new Random();
        private int sessionHash;

        private int GenerateRandomSessionHash()
        {
            return randomNumberGenerator.Next(0, 100000001);
        }

        public PriorityVotes Start(string pathOnDisk)
        {
            sessionHash = GenerateRandomSessionHash();
            var priorityVotesFilesDictionary = ParseRootElement(pathOnDisk);

            if (priorityVotesFilesDictionary == null)
            {
                return null;
            }

            crawlerHelpers.DownloadPages(priorityVotesFilesDictionary);
            return ParsePriorityVotesPages(priorityVotesFilesDictionary);
        }

        public Dictionary<string, string> ParseRootElement(string pathOnDisk)
        {
            var priorityVotesFilesDictionary = new Dictionary<string, string>();

            htmlDocument.Load(pathOnDisk, Encoding.UTF8);
            var pages = htmlDocument.DocumentNode.SelectNodes("//table[@class='partydata']/tbody/tr/td/a");
            var documentIndex = 2000;
            if (pages == null)
            {
                return null;
            }

            foreach (var page in pages)
            {
                var url = string.Format("http://www.vrk.lt{0}", page.Attributes["href"].Value);
                if (url.Contains("partijos_balsai_apygardoje") || url.Contains("gauti_balsai_apygardoje") || !url.Contains("pirmumo_balsai"))
                {
                    continue;
                }
                
                priorityVotesFilesDictionary.Add(url, crawlerHelpers.GetFilePathOnDisk(sessionHash));
                documentIndex++;
            }

            return priorityVotesFilesDictionary;
        }

        public PriorityVotes ParsePriorityVotesPages(Dictionary<string, string> fileDictionary)
        {
            var priorityVotes = new PriorityVotes();
            foreach (var file in fileDictionary)
            {
                priorityVotes.ListOfParties.Add(CreateCandidateList(file));
            }

            return priorityVotes;
        }

        public Party CreateCandidateList(KeyValuePair<string, string> file)
        {
            var listOfCandidates = new List<Candidate>();
            htmlDocument.Load(file.Value, Encoding.UTF8);
            var partyHeaderGlobal = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[3]/h2");
            var partyHeaderLocal = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center/center[3]/h2");
            var party = string.Empty;
            if (partyHeaderGlobal != null)
            {
                party = partyHeaderGlobal.InnerText;
            }
            else
            {
                party = partyHeaderLocal.InnerText;
            }

            var candidates = htmlDocument.DocumentNode.SelectNodes("//table[@class='partydata' and @width='70%' and @border='0']/tbody/tr");
            candidates.RemoveAt(0);
            foreach (var candidate in candidates)
            {
                if (candidate.ChildNodes[1].InnerText == "-")
                {
                    continue;
                }

                var rankAfterElections = Convert.ToInt32(candidate.ChildNodes[1].InnerText);
                var name = candidate.ChildNodes[3].InnerText.Trim();
                var rankBeforeElections = Convert.ToInt32(candidate.ChildNodes[5].InnerText);
                var priorityVotes = Convert.ToInt32(candidate.ChildNodes[7].InnerText);
                var amountOfPoints = 0;
                if (candidate.ChildNodes.Count > 9)
                {
                    amountOfPoints = Convert.ToInt32(candidate.ChildNodes[9].InnerText);
                }

                listOfCandidates.Add(new Candidate(rankBeforeElections, rankAfterElections, name, priorityVotes, amountOfPoints, party.Replace("&quot;", "")));
            }

            return new Party(listOfCandidates);
        }
    }
}
