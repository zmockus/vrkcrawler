﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using Newtonsoft.Json;
using ServiceStack.Text;
using VRKCrawlerManager.Helpers;
using VRKCrawlerManager.Interfaces;
using VRKCrawlerManager.Model.MultimandateElections;
using VRKCrawlerManager.Model.MultimandateElections.PriorityVotes;

namespace VRKCrawlerManager.Services.ParliamentElections
{
    public class ParliamentElectionsMultimandateRound1CrawlerService : ICrawlerService
    {
        private readonly IPriorityVotesCrawlerService priorityVotesCrawlerService = new ParliamentElectionsRound1PriorityVotesCrawlerService();
        private static readonly HtmlDocument htmlDocument = new HtmlDocument();
        private readonly Random randomNumberGenerator;
        private readonly int sessionHash;
        private readonly CrawlerHelpers crawlerHelpers;

        private string RootUrl;
        private string RootUrlEmbassies;
        private string Year;
        private int AmountOfParties;

        public ParliamentElectionsMultimandateRound1CrawlerService(string rootUrl, string rootUrlEmbassies,
            string year, int amountOfParties, CancellationToken token)
        {
            RootUrl = rootUrl;
            RootUrlEmbassies = rootUrlEmbassies;
            Year = year;
            AmountOfParties = amountOfParties;
            crawlerHelpers = new CrawlerHelpers(token);
            randomNumberGenerator = new Random();
            sessionHash = GenerateRandomSessionHash();
        }

        private int GenerateRandomSessionHash()
        {
            return randomNumberGenerator.Next(0, 100000001);
        }

        public void Start()
        {
            var countryResults = new Country();

            countryResults.Embassies = ProcessEmbassyResults();

            var rootFileDictionary = new Dictionary<string, string>
            {
                {RootUrl, crawlerHelpers.GetFilePathOnDisk(sessionHash)}
            };

            crawlerHelpers.DownloadPages(rootFileDictionary);
            var filePathOnDisk = rootFileDictionary.Values.First();
            countryResults.PriorityVotes = priorityVotesCrawlerService.Start(filePathOnDisk);
            countryResults.Results = ParseRootResults(filePathOnDisk);
            var municipalityDictionary = FillMunicipalityDictionary(filePathOnDisk);

            crawlerHelpers.DownloadPages(municipalityDictionary);
            countryResults.Municipalities = ProcessMunicipalityAndVicinityResults(municipalityDictionary);

            var jsonFileName = string.Format("ParliamentElection{0}Round1Multimandate.json", Year);
            var csvFileName = string.Format("ParliamentElection{0}Round1Multimandate.csv", Year);
            var primaryVotesCsvFileName = string.Format("ParliamentElection{0}Round1MultimandatePriorityVotes.csv", Year);

            File.Create(jsonFileName).Dispose();
            File.AppendAllText(jsonFileName, JsonConvert.SerializeObject(countryResults, Formatting.None));

            File.Create(primaryVotesCsvFileName).Dispose();
            var candidateList = CrawlerHelpers.GetCsvListPriorityVotes(countryResults);
            File.AppendAllText(primaryVotesCsvFileName, CsvSerializer.SerializeToCsv<Candidate>(candidateList));

            File.Create(csvFileName).Dispose();
            var partyResults = CrawlerHelpers.CreateCsvListMultimandate(countryResults);
            File.AppendAllText(csvFileName, CsvSerializer.SerializeToCsv<PartyResults>(partyResults));
        }

        public Results ParseRootResults(string filePathOnDisk)
        {
            htmlDocument.Load(filePathOnDisk, Encoding.UTF8);

            var results = new Results
            {
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[5]").InnerText),
                TotalAmountOfVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[6]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[10]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[8]").InnerText)
            };

            var partyResults = htmlDocument.DocumentNode.SelectNodes("//div[@id='nFooter']/div/table/tbody/tr/td/table[2]/tbody/tr");
            partyResults.RemoveAt(0);
            partyResults.RemoveAt(0);
            foreach (var partyResult in partyResults)
            {
                var partyName = partyResult.ChildNodes[3].InnerText.Trim();
                var totalAmountOfVotesGivenInDistricts = Convert.ToInt32(partyResult.ChildNodes[7].InnerText);
                var totalAmountOfVotesGivenUsingPost = Convert.ToInt32(partyResult.ChildNodes[9].InnerText);
                var amountOfMandatesReceived = Convert.ToInt32(partyResult.ChildNodes[17].InnerText);

                results.ListOfParties.Add(new PartyResults(partyName, totalAmountOfVotesGivenUsingPost, totalAmountOfVotesGivenInDistricts, results.TotalAmountOfVoters, results.TotalAmountOfValidVotes, amountOfMandatesReceived));

                if (results.ListOfParties.Count == AmountOfParties)
                {
                    break;
                }
            }
            
            return results;
        }

        public Dictionary<string, string> FillMunicipalityDictionary(string filePathOnDisk)
        {
            htmlDocument.Load(filePathOnDisk, Encoding.UTF8);
            var dictionary = new Dictionary<string, string>();

            var pages = htmlDocument.DocumentNode.SelectNodes("//table[@class='partydata']/tbody/tr/td/b/a");
            var documentIndex = 1;
            foreach (var page in pages)
            {
                var url =
                    string.Format("http://www.vrk.lt{0}", page.Attributes["href"].Value);

                dictionary.Add(url, crawlerHelpers.GetFilePathOnDisk(sessionHash));
                documentIndex++;
            }

            return dictionary;
        }

        public List<Municipality> ProcessMunicipalityAndVicinityResults(Dictionary<string, string> municipalitiesDictionary)
        {
            var municipalitiesList = new List<Municipality>();

            foreach (var municipalityPage in municipalitiesDictionary)
            {
                htmlDocument.Load(municipalityPage.Value, Encoding.UTF8);
                var municipality = ExtractMunicipalityResultsAndPriorityVotes(municipalityPage);
                var vicinitiesDictionary = FillVicinityDictionary();
                crawlerHelpers.DownloadPages(vicinitiesDictionary);
                foreach (var vicinityItem in vicinitiesDictionary)
                {
                    htmlDocument.Load(vicinityItem.Value, Encoding.UTF8);
                    var vicinity = ExtractVicinityResultsAndPriorityVotes(vicinityItem);
                    if (vicinity == null)
                    {
                        continue;
                    }

                    municipality.ListOfVicinities.Add(vicinity);
                }

                municipalitiesList.Add(municipality);
            }

            return municipalitiesList;
        }

        public Municipality ExtractMunicipalityResultsAndPriorityVotes(KeyValuePair<string, string> municipalityPage)
        {
            var municipality = new Municipality();
            municipality.PriorityVotes = priorityVotesCrawlerService.Start(municipalityPage.Value);
            municipality.Results = new Results
            {
                Description = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[1]/h2").InnerText,
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[2]").InnerText),
                TotalAmountOfVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[3]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[7]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[5]").InnerText)
            };

            var partyResults = htmlDocument.DocumentNode.SelectNodes("//div[@id='nFooter']/div/table/tbody/tr/td/table[2]/tbody/tr");
            partyResults.RemoveAt(0);
            partyResults.RemoveAt(0);
            foreach (var partyResult in partyResults)
            {
                var partyName = partyResult.ChildNodes[3].InnerText.Trim();
                var totalAmountOfVotesGivenInDistricts = Convert.ToInt32(partyResult.ChildNodes[7].InnerText);
                var totalAmountOfVotesGivenUsingPost = Convert.ToInt32(partyResult.ChildNodes[9].InnerText);
                var amountOfMandatesReceived = 0;

                municipality.Results.ListOfParties.Add(new PartyResults(partyName, totalAmountOfVotesGivenUsingPost,
                    totalAmountOfVotesGivenInDistricts, municipality.Results.TotalAmountOfVotes,
                    municipality.Results.TotalAmountOfValidVotes, amountOfMandatesReceived));

                if (municipality.Results.ListOfParties.Count == AmountOfParties)
                {
                    break;
                }
            }

            return municipality;
        }

        private Dictionary<string, string> FillVicinityDictionary()
        {
            var vicinitiesDictionary = new Dictionary<string, string>();

            var vicinities = htmlDocument.DocumentNode.SelectNodes("//div[@id='nFooter']/div/table/tbody/tr/td/table[3]/tbody/tr/td/a");
            var documentIndex = 200;
            foreach (var vicinity in vicinities)
            {
                var url = string.Format("http://www.vrk.lt{0}", vicinity.Attributes["href"].Value);
                if (url.Contains("balsavimasatstovybese"))
                {
                    continue;
                }

                vicinitiesDictionary.Add(url, crawlerHelpers.GetFilePathOnDisk(sessionHash));
                documentIndex++;
            }

            return vicinitiesDictionary;
        }



        public Vicinity ExtractVicinityResultsAndPriorityVotes(KeyValuePair<string, string> vicinityItem)
        {
            var vicinity = new Vicinity();
            vicinity.PriorityVotes = priorityVotesCrawlerService.Start(vicinityItem.Value);
            vicinity.Results = new Results
            {
                Description = string.Format("{0} {1}", htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[1]/h2[1]/a").InnerText,
                        htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[1]/h2[2]").InnerText),
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[1]").InnerText),
                TotalAmountOfVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[2]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[6]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[4]").InnerText)
            };

            if (vicinity.Results.TotalAmountOfVoters == 0)
            {
                return null;
            }

            var partyResults = htmlDocument.DocumentNode.SelectNodes("//div[@id='nFooter']/div/table/tbody/tr/td/table[2]/tbody/tr");
            partyResults.RemoveAt(0);
            partyResults.RemoveAt(0);
            if (partyResults.Count <= 1)
            {
                return null;
            }

            foreach (var partyResult in partyResults)
            {
                var partyName = partyResult.ChildNodes[3].InnerText.Trim();
                var totalAmountOfVotesGivenInDistricts = Convert.ToInt32(partyResult.ChildNodes[7].InnerText);
                var totalAmountOfVotesGivenUsingPost = Convert.ToInt32(partyResult.ChildNodes[9].InnerText);
                var amountOfMandatesReceived = 0;

                vicinity.Results.ListOfParties.Add(new PartyResults(partyName, totalAmountOfVotesGivenUsingPost,
                    totalAmountOfVotesGivenInDistricts, vicinity.Results.TotalAmountOfVotes,
                    vicinity.Results.TotalAmountOfValidVotes, amountOfMandatesReceived));

                if (vicinity.Results.ListOfParties.Count == AmountOfParties)
                {
                    break;
                }
            }

            return vicinity;
        }

        public Municipality ProcessEmbassyResults()
        {
            var rootFileDictionary = new Dictionary<string, string>
            {
                {RootUrlEmbassies, crawlerHelpers.GetFilePathOnDisk(sessionHash)}
            };

            crawlerHelpers.DownloadPages(rootFileDictionary);

            return ProcessMunicipalityAndVicinityResults(rootFileDictionary).First();
        } 
    }
}
