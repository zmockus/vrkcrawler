﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using Newtonsoft.Json;
using ServiceStack.Text;
using VRKCrawlerManager.Helpers;
using VRKCrawlerManager.Interfaces;
using VRKCrawlerManager.Model.SinglemandateElections;

namespace VRKCrawlerManager.Services.ParliamentElections
{
    public class ParliamentElectionsSinglemandateRound1CrawlerService : ICrawlerService
    {
        private static readonly HtmlDocument htmlDocument = new HtmlDocument();
        private readonly Random randomNumberGenerator;
        private readonly int sessionHash;
        private CrawlerHelpers crawlerHelpers;

        private string RootUrl;
        private string RootUrlEmbassies;
        private string Year;

        public ParliamentElectionsSinglemandateRound1CrawlerService(string rootUrl, string rootUrlEmbassies,
            string year, CancellationToken token)
        {
            RootUrl = rootUrl;
            RootUrlEmbassies = rootUrlEmbassies;
            Year = year;
            crawlerHelpers = new CrawlerHelpers(token);
            randomNumberGenerator = new Random();
            sessionHash = GenerateRandomSessionHash();
        }

        private int GenerateRandomSessionHash()
        {
            return randomNumberGenerator.Next(0, 100000001);
        }

        public void Start()
        {
            var countryResults = new Country();

            countryResults.ResultsFromEmbassies = GetResultsFromEmbassies();

            var rootFileDictionary = new Dictionary<string, string>
            {
                {RootUrl, crawlerHelpers.GetFilePathOnDisk(sessionHash)}
            };

            crawlerHelpers.DownloadPages(rootFileDictionary);
            countryResults.Results = ParseRootResults(rootFileDictionary.Values.First());
            var municipalityDictionary = FillMunicipalityDictionary(rootFileDictionary.Values.First());

            crawlerHelpers.DownloadPages(municipalityDictionary);
            ProcessMunicipalityResults(municipalityDictionary, countryResults);

            var jsonFileName = string.Format("ParliamentElection{0}SinglemandateRound1.json", Year);
            var csvFileName = string.Format("ParliamentElection{0}SinglemandateRound1.csv", Year);

            File.Create(jsonFileName).Dispose();
            File.AppendAllText(jsonFileName, JsonConvert.SerializeObject(countryResults, Formatting.Indented));

            var candidateResults = CrawlerHelpers.CreateCsvListSiglemandate(countryResults);
            File.AppendAllText(csvFileName, CsvSerializer.SerializeToCsv<CandidateResults>(candidateResults));
        }


        public Results ParseRootResults(string pathOnDisk)
        {
            htmlDocument.Load(pathOnDisk, Encoding.UTF8);

            var results = new Results
            {
                Description = htmlDocument.DocumentNode.SelectSingleNode("//h2[@id='ataskaita']").InnerText,
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[3]").InnerText),
                TotalAmountOfVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[4]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[6]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[8]").InnerText)
            };

            return results;
        }


        public Dictionary<string, string> FillMunicipalityDictionary(string pathOnDisk)
        {
            var municipalityDictionary = new Dictionary<string, string>();

            htmlDocument.Load(pathOnDisk, Encoding.UTF8);
            var districts = htmlDocument.DocumentNode.SelectNodes("//table[@class='partydata']/tbody/tr/td/b/a");
            var documentIndex = 1;
            foreach (var district in districts)
            {
                var url =
                    string.Format(
                        "http://www.vrk.lt{0}",
                        district.Attributes["href"].Value);
                municipalityDictionary.Add(url, crawlerHelpers.GetFilePathOnDisk(sessionHash));
                documentIndex++;
            }

            return municipalityDictionary;
        }

        public void ProcessMunicipalityResults(Dictionary<string, string> dictionary, Country country)
        {
            country.ListOfMunicipalities = new List<Municipality>();

            foreach (var item in dictionary)
            {
                country.ListOfMunicipalities.Add(ParseMunicipalityResults(item.Value));
            }
        }

        public Municipality ParseMunicipalityResults(string pathOnDisk)
        {
            var municipality = new Municipality();
            htmlDocument.Load(pathOnDisk, Encoding.UTF8);

            var candidateNodes = htmlDocument.DocumentNode.SelectNodes("//div[@id='nFooter']/div/table/tbody/tr/td/table[2]/tbody/tr");
            candidateNodes.RemoveAt(candidateNodes.Count - 1);
            candidateNodes.RemoveAt(0);
            candidateNodes.RemoveAt(0);

            var results = new Results
            {
                Description = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[1]/h2").InnerText,
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[2]").InnerText),
                TotalAmountOfVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[3]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[5]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[7]").InnerText)
            };

            foreach (var candidateNode in candidateNodes)
            {
                var candidate = new CandidateResults
                {
                    Name = candidateNode.ChildNodes[1].InnerText.Trim().Replace("\r\n", ""),
                    AmountOfVotesGivenInDistricts = Convert.ToInt32(candidateNode.ChildNodes[3].InnerText),
                    AmountOfVotesGivenUsingPost = Convert.ToInt32(candidateNode.ChildNodes[5].InnerText),
                    TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[3]").InnerText),
                    TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/table[1]/tbody/tr/td/b[7]").InnerText)
                };

                results.ListOfCandidates.Add(candidate);
            }
            
            municipality.Results = results;

            var vicinityDictionaryForAMunicipality = FillVicinityDictionary(pathOnDisk);
            crawlerHelpers.DownloadPages(vicinityDictionaryForAMunicipality);

            municipality.ListOfVicinities = ProcessVicinityResults(vicinityDictionaryForAMunicipality);

            return municipality;
        }

        public Dictionary<string, string> FillVicinityDictionary(string pathOnDisk)
        {
            var vicinity = new Dictionary<string, string>();

            htmlDocument.Load(pathOnDisk, Encoding.UTF8);
            var districts = htmlDocument.DocumentNode.SelectNodes("//table[@class='partydata'][3]/tbody/tr/td/a");
            var documentIndex = 100;
            foreach (var district in districts)
            {
                if (district.InnerText == "Balsavimas LR diplomatinėse atstovybėse")
                {
                    continue;
                }

                var url =
                    string.Format(
                        "http://www.vrk.lt{0}",
                        district.Attributes["href"].Value);
                vicinity.Add(url, crawlerHelpers.GetFilePathOnDisk(sessionHash));
                documentIndex++;
            }

            return vicinity;
        }

        public List<Vicinity> ProcessVicinityResults(Dictionary<string, string> vicinityDictionary)
        {
            var listOfVicinities = new List<Vicinity>();
            foreach (var vicinityItem in vicinityDictionary)
            {
                var vicinity = new Vicinity { Results = ParseVicinityResults(vicinityItem.Value) };
                listOfVicinities.Add(vicinity);
            }

            return listOfVicinities;
        }

        public Results ParseVicinityResults(string pathOnDisk)
        {
            htmlDocument.Load(pathOnDisk, Encoding.UTF8);

            var results = new Results
            {
                Description = string.Format("{0}-{1}", htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/p/font/b/a").InnerText,
                htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[1]/h2").InnerText),
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[3]/b[1]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[3]/b[4]").InnerText)
                + Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[3]/b[6]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[3]/b[3]").InnerText)
                + Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tbody/tr/td/center[3]/b[5]").InnerText)
            };

            var candidateNodes = htmlDocument.DocumentNode.SelectNodes("//div[@id='nFooter']/div/table/tbody/tr/td/table/tbody/tr");
            candidateNodes.RemoveAt(candidateNodes.Count - 1);
            candidateNodes.RemoveAt(0);
            candidateNodes.RemoveAt(0);

            foreach (var candidateNode in candidateNodes)
            {
                var candidate = new CandidateResults
                {
                    Name = candidateNode.ChildNodes[1].InnerText.Trim().Replace("\r\n", ""),
                    AmountOfVotesGivenInDistricts = Convert.ToInt32(candidateNode.ChildNodes[3].InnerText),
                    AmountOfVotesGivenUsingPost = Convert.ToInt32(candidateNode.ChildNodes[5].InnerText),
                    TotalAmountOfVoters = results.TotalAmountOfVoters,
                    TotalAmountOfValidVotes = results.TotalAmountOfValidVotes
                };

                results.ListOfCandidates.Add(candidate);
            }

            results.TotalAmountOfVotes = results.TotalAmountOfValidVotes + results.TotalAmountOfInvalidVotes;

            return results;
        }

        public Municipality GetResultsFromEmbassies()
        {
            var rootFileDictionary = new Dictionary<string, string>
            {
                {RootUrlEmbassies, crawlerHelpers.GetFilePathOnDisk(sessionHash)}
            };

            crawlerHelpers.DownloadPages(rootFileDictionary);

            var embassyData = ParseMunicipalityResults(rootFileDictionary.Values.First());

            return embassyData;
        }
    }
}
