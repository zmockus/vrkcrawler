﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using Newtonsoft.Json;
using ServiceStack.Text;
using VRKCrawlerManager.Helpers;
using VRKCrawlerManager.Interfaces;
using VRKCrawlerManager.Model.SinglemandateElections;

namespace VRKCrawlerManager.Services.PresidentElections
{
    public class PresidentElectionsRound2CrawlerService : ICrawlerService
    {
        private static readonly HtmlDocument htmlDocument = new HtmlDocument();
        private readonly Random randomNumberGenerator;
        private readonly int sessionHash;
        private CrawlerHelpers crawlerHelpers;

        private string RootUrl;
        private string MunicipalityUrl;
        private string VicinityUrl;
        private string Year;

        public PresidentElectionsRound2CrawlerService()
        {
        }

        public PresidentElectionsRound2CrawlerService(string rootUrl, string municipalityUrl, string vicinityUrl, string year, CancellationToken token)
        {
            RootUrl = rootUrl;
            MunicipalityUrl = municipalityUrl;
            VicinityUrl = vicinityUrl;
            Year = year;
            crawlerHelpers = new CrawlerHelpers(token);
            randomNumberGenerator = new Random();
            sessionHash = GenerateRandomSessionHash();
        }

        private int GenerateRandomSessionHash()
        {
            return randomNumberGenerator.Next(0, 100000001);
        }

        public void Start()
        {
            var countryResults = new Country();

            var rootFileDictionary = new Dictionary<string, string>
            {
                {RootUrl, crawlerHelpers.GetFilePathOnDisk(sessionHash)}
            };

            crawlerHelpers.DownloadPages(rootFileDictionary);
            countryResults.Results = ParseRootResults(rootFileDictionary.Values.First());
            var municipalityDictionary = FillMunicipalityDictionary(rootFileDictionary.Values.First());

            crawlerHelpers.DownloadPages(municipalityDictionary);
            ProcessMunicipalityResults(municipalityDictionary, countryResults);

            var csvFileName = string.Format("PresidentElection{0}Round2.csv", Year);
            var jsonFileName = string.Format("PresidentElection{0}Round2.json", Year);

            File.Create(csvFileName).Dispose();

            var candidateResults = CrawlerHelpers.CreateCsvListSiglemandate(countryResults);

            File.AppendAllText(csvFileName, CsvSerializer.SerializeToCsv<CandidateResults>(candidateResults));

            File.Create(jsonFileName).Dispose();

            File.AppendAllText(jsonFileName, JsonConvert.SerializeObject(countryResults, Formatting.Indented));
        }


        public Results ParseRootResults(string pathOnDisk)
        {
            htmlDocument.Load(pathOnDisk, Encoding.UTF8);

            var candidateOne = new CandidateResults
                               {
                                   Name = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/table/tr[3]/td[1]/a").InnerText.Replace("\r\n", ""),
                                   AmountOfVotesGivenInDistricts = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/table/tr[3]/td[2]").InnerText),
                                   AmountOfVotesGivenUsingPost = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/table/tr[3]/td[3]").InnerText),
                                   TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[4]").InnerText),
                                   TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[8]").InnerText)
                               };

            var candidateTwo = new CandidateResults
                               {
                                   Name = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/table/tr[4]/td[1]/a").InnerText.Replace("\r\n", ""),
                                   AmountOfVotesGivenInDistricts = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/table/tr[4]/td[2]").InnerText),
                                   AmountOfVotesGivenUsingPost = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/table/tr[4]/td[3]").InnerText),
                                   TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[4]").InnerText),
                                   TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[8]").InnerText)
                               };

            var results = new Results
                          {
                                Description = htmlDocument.DocumentNode.SelectSingleNode("//h2[@id='ataskaita']").InnerText,
                                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[3]").InnerText),
                                TotalAmountOfVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[4]").InnerText),
                                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[6]").InnerText),
                                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[8]").InnerText)
                          };

            results.ListOfCandidates.Add(candidateOne);
            results.ListOfCandidates.Add(candidateTwo);

            return results;
        }


        public Dictionary<string, string> FillMunicipalityDictionary(string pathOnDisk)
        {
            var municipalityDictionary = new Dictionary<string, string>();

            htmlDocument.Load(pathOnDisk, Encoding.UTF8);
            var districts = htmlDocument.DocumentNode.SelectNodes("//table[@class='partydata']/tr/td/b/a");
            var documentIndex = 1;
            foreach (var district in districts)
            {
                var url =
                    string.Format(
                        MunicipalityUrl,
                        district.Attributes["href"].Value);
                municipalityDictionary.Add(url, crawlerHelpers.GetFilePathOnDisk(sessionHash));
                documentIndex++;
            }

            return municipalityDictionary;
        }

        public void ProcessMunicipalityResults(Dictionary<string, string> dictionary, Country country)
        {
            country.ListOfMunicipalities = new List<Municipality>();

            foreach (var item in dictionary)
            {
                country.ListOfMunicipalities.Add(ParseMunicipalityResults(item.Value));
            }
        }

        public Municipality ParseMunicipalityResults(string pathOnDisk)
        {
            var municipality = new Municipality();
            htmlDocument.Load(pathOnDisk, Encoding.UTF8);

            var candidateOne = new CandidateResults
            {
                Name = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[3]/td[1]/a").InnerText.Replace("\r\n", ""),
                AmountOfVotesGivenInDistricts = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[3]/td[2]").InnerText),
                AmountOfVotesGivenUsingPost = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[3]/td[3]").InnerText),
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[2]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[7]").InnerText)
            };

            var candidateTwo = new CandidateResults
            {
                Name = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[4]/td[1]/a").InnerText.Replace("\r\n", ""),
                AmountOfVotesGivenInDistricts = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[4]/td[2]").InnerText),
                AmountOfVotesGivenUsingPost = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[4]/td[3]").InnerText),
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[2]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[7]").InnerText)
            };

            var results = new Results
            {
                Description = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[1]/h2").InnerText,
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[2]").InnerText),
                TotalAmountOfVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[3]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[5]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div/table/tr/td/table/tr/td/b[7]").InnerText)
            };

            results.ListOfCandidates.Add(candidateOne);
            results.ListOfCandidates.Add(candidateTwo);

            municipality.Results = results;

            var vicinityDictionaryForAMunicipality = FillVicinityDictionary(pathOnDisk);
            crawlerHelpers.DownloadPages(vicinityDictionaryForAMunicipality);

            municipality.ListOfVicinities = ProcessVicinityResults(vicinityDictionaryForAMunicipality);

            return municipality;
        }

        public Dictionary<string, string> FillVicinityDictionary(string pathOnDisk)
        {
            var vicinity = new Dictionary<string, string>();

            htmlDocument.Load(pathOnDisk);
            var districts = htmlDocument.DocumentNode.SelectNodes("//table[@class='partydata'][3]/tr/td/a");
            var documentIndex = 100;
            foreach (var district in districts)
            {
                var url =
                    string.Format(
                        VicinityUrl,
                        district.Attributes["href"].Value);
                vicinity.Add(url, crawlerHelpers.GetFilePathOnDisk(sessionHash));
                documentIndex++;
            }

            return vicinity;
        }

        public List<Vicinity> ProcessVicinityResults(Dictionary<string, string> vicinityDictionary)
        {
            var listOfVicinities = new List<Vicinity>();
            foreach (var vicinityItem in vicinityDictionary)
            {
                var vicinity = new Vicinity { Results = ParseVicinityResults(vicinityItem.Value) };
                listOfVicinities.Add(vicinity); 
            }

            return listOfVicinities;
        }

        public Results ParseVicinityResults(string pathOnDisk)
        {
            htmlDocument.Load(pathOnDisk, Encoding.UTF8);

            var candidateOne = new CandidateResults
            {
                Name = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[3]/td[1]/a").InnerText.Replace("\r\n", ""),
                AmountOfVotesGivenInDistricts = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[3]/td[2]").InnerText),
                AmountOfVotesGivenUsingPost = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[3]/td[3]").InnerText),
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/b[1]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[5]/th[4]/b").InnerText)
            };

            var candidateTwo = new CandidateResults
            {
                Name = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[4]/td[1]/a").InnerText.Replace("\r\n", ""),
                AmountOfVotesGivenInDistricts = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[4]/td[2]").InnerText),
                AmountOfVotesGivenUsingPost = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[4]/td[3]").InnerText),
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/b[1]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[5]/th[4]/b").InnerText)
            };

            var results = new Results
            {
                Description = string.Format("{0} {1}", htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/p/font/b/a").InnerText,
                htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[1]/h2").InnerText),
                TotalAmountOfVoters = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/b[1]").InnerText),
                TotalAmountOfInvalidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/b[4]").InnerText) +
                Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/center[3]/b[6]").InnerText),
                TotalAmountOfValidVotes = Convert.ToInt32(htmlDocument.DocumentNode.SelectSingleNode("//div[@id='nFooter']/div[2]/table/tr/td/table/tr[5]/th[4]/b").InnerText)
            };

            results.TotalAmountOfVotes = results.TotalAmountOfValidVotes + results.TotalAmountOfInvalidVotes;

            results.ListOfCandidates.Add(candidateOne);
            results.ListOfCandidates.Add(candidateTwo);

            return results;
        }
    }
}
